<?php

    /*
        El bucle Do While
        En caso de que la condición se cumpla, se volverá a repetir este proceso hasta que se deje de cumplir la condición, 
        mientras que en el bucle While se comprueba la condición antes de ejecutar el códig
    */

    $contador=11;
    
    do {
        echo '1 * ' . $contador. ' = '.(1*$contador).'<br>';    
        $contador++;

    } while ($contador <= 10);
    
    die();

    $contador = 1;
    do {
        
        echo '1 * ' . $contador. ' = '.(1*$contador).'<br>';    
        $contador++;
        
    } while ($contador <= 10);

    die();

    

?>