<?php

/*
    Variables numéricas
    Enteros $entero=2002; Numeros sin decimales
    Reales $real=3.14159; Numeros con o sin decimal
*/

$entero = 2000;
$real = '3.14159';

//echo $entero + $real;


/*
    Variables alfanuméricas
    Cadenas Almacenan variables alfanuméricas $cadena="Hola amigo";
*/

$cadena_doble_comilla="Bienvenidos";
$cadena_simple = 'UGI/SESAL al Curso Básico de PHO';


$cadena_compuesta = "{$cadena_doble_comilla} {$cadena_simple} Día 2";
$cadena_compuesta = $cadena_doble_comilla. ' ' .$cadena_simple . ' Día 2';


/*
    Boleanas

    Boleano verdadero $verdadero = true;
    Boleano falso $falso = false;
*/

$verdadero = TRUE;
$falso = FALSE;


/*
    Matrices, tablas o arrays
    Es un tipo de datos en el que, en lugar de tener un dato, podemos almacenar un conjunto de ellos, a los que accedemos a través de índices.
*/

$sentido[1]="ver";
$sentido[2]="tocar";
$sentido[3]="oir";
$sentido[4]="gusto";
$sentido[5]="oler";

$sentido = ['ver', 'tocar', 'oir', 'gusto', 'oler'];
$sentido = array('ver', 'tocar', 'oir', 'gusto', 'oler');


/*
    Objetos
    Se trata de conjuntos de variables y funciones asociadas. Presentan una complejidad mayor que las variables anterirores
*/

class Carro {
    public $color;
    public $modelo;


}

$miCarro = new Carro;


//TIPADO DINAMICO


$cadena ="5"; //esto es una cadena
$entero =3; //esto es un entero

//echo $cadena+$entero;



//PHP es sensible a las mayúsculas y minúsculas
$mi_variable_test = 'Hola PHP';
$Mi_variable_test = 'Variable diferente';

//echo $Mi_variable_test;



//Variables asignadas por referencia

$variable_original = 'Juan'; // Asigna el valor 'Juan' a $variable_original 
$variable_modificada = &$variable_original; // Referencia $variable_original vía $variable_modificada. 
$variable_modificada = "Mi nombre es Ana"; // Modifica $variable_modificada... 

echo $variable_modificada; // $variable_original también se modifica. 
echo '<br>';
echo $variable_original; 


?>
