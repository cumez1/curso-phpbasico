<?php

class User
{
    //Diccionario de Roles

    const ROL_ADMINISTRADOR = 1;
    const SECURITY_ROL = 2;
    const CUSTOMER_ROL = 3;
    const CONSULTA_ROL = 4;

    private $id;

    private $nombre;

    private $age;

    private $rol;

    public function __construct($id, $nombre, $age)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->age = $age;
        $this->rol = 1;
    }

    /**
     * Nicolas Cumez
     * 2023-03-23
     * cumez.1@gmail.com
     */
    public function getID()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->nombre;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getRol()
    {
        return $this->rol;
    }
}

$user = new User(10, "Juan", 30);

echo $user->getRol() == User::ROL_ADMINISTRADOR ? "Admin" : "Otro";

