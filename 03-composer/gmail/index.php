<?php
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
//https://mailtrap.io/blog/phpmailer-gmail/
//Para usar debemos habilitar https://console.cloud.google.com/apis/credentials/consent?hl=es-419&project=icap-latam


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer;

try {

    $mail->isSMTP(); // Usa SMTP
    $mail->SMTPAuth = true; // Habilita la autenticación SMTP
    $mail->SMTPSecure = 'tls'; // Habilita la seguridad TLS
    $mail->Host = 'smtp.gmail.com'; // Especifica el servidor SMTP de Gmail
    $mail->Port = 587; // Puerto SMTP de Gmail
    $mail->Username = 'gmail.com'; //SMTP username
    $mail->Password = 'passs';  //SMTP password

    //Recipients
    $mail->setFrom('itest@gmail.com', 'ICAP Guatemala');
    $mail->addAddress('test@cumc.columbia.edu', 'Nicolas Cumez');     //Add a recipient
   
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');


    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = 'Asunto Composer';
    $mail->Body    = 'Prueba de correo electronico <b>via PHP MAILER!</b>';
    $mail->AltBody = 'Esto es el cuerpo del mensaje';

    $response = $mail->send();
    
    if ($response) {
        echo 'Se ha enviado exitosamente el correo';
    } else {
        echo 'No se pudo enviar el correo';
    }

} catch (Exception $e ) {
    echo $e->getMessage();
    echo "Error al enviar correo: {$mail->ErrorInfo}";
}